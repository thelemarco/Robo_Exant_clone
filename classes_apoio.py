import urllib3
import requests
from Levenshtein import distance
import pygetwindow as gw
import time
from pywinauto.application import Application

class Util_suport:

    def converter_string_para_strings(self, string_valor):
        # Remover caracteres indesejados: espaços e símbolos monetários
        caracteres_indesejados = [' ', 'R$', '€', '$', '¥', '£']
        for caractere in caracteres_indesejados:
            string_valor = string_valor.replace(caractere, '')

        # Substituir a vírgula por ponto se estiver presente
        string_valor = string_valor.replace(',', '.')

        # Separar os números antes e depois do ponto flutuante
        partes = string_valor.split('.')
        numero_esquerda = str(partes[0])
        numero_direita = partes[1] if len(partes) > 1 else '00'

        # Garantir que a string à direita tenha duas casas decimais
        numero_direita = numero_direita.ljust(2, '0')

        return numero_esquerda, numero_direita


    def converter_string_para_float(self, string_valor):
        # Remover caracteres indesejados: espaços, símbolos monetários e pontos
        caracteres_indesejados = [' ', 'R$', '€', '$', '¥', '£']
        for caractere in caracteres_indesejados:
            string_valor = string_valor.replace(caractere, '')

        # Remover caracteres não numéricos
        string_valor = ''.join(filter(str.isdigit, string_valor))

        # Substituir a vírgula por ponto
        string_valor = string_valor.replace(',', '.')

        # Converter a string em float e dividir por 100
        valor_float = float(string_valor) / 100

        # Formatar o valor com duas casas decimais
        valor_formatado = "{:.2f}".format(valor_float)

        return valor_formatado


    def remover_posicoes(self, string, posicoes):
        # Converter a string em uma lista de caracteres
        caracteres = list(string)

        # Remover os caracteres nas posições especificadas
        for posicao in sorted(posicoes, reverse=True):
            del caracteres[posicao]

        # Juntar os caracteres restantes em uma string novamente
        nova_string = ''.join(caracteres)
        return nova_string

    def verificar_servico(self, url):
        try:
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
            response = requests.get(url, verify=False)
            if response.status_code == 200:
                print("O serviço está disponível e estável.")
            else:
                print(f"O serviço retornou o código de status: {response.status_code}")
        except requests.exceptions.RequestException as e:
            print("Não foi possível acessar o serviço:", e)


    def extrair_numerais(self, texto):
        numerais = ''
        for caractere in texto:
            if caractere.isdigit():
                numerais += caractere
        return numerais


    def compara_nomes(self, nome1, nome2):

        teste1 = nome1.strip().upper().replace(" ", "")
        teste2 = nome2.strip().upper().replace(" ", "")

        # Calcular a distância de Levenshtein
        dist = distance(teste1, teste2)

        #print("A distância de Levenshtein é", dist)

        # Decidir com base na distância
        if dist <= 3:
            return True
        else:
            return False

    def primeiro_plano(self, navegador):
        navegador.set_window_size(800, 600)
        time.sleep(1)  # Deixa algum tempo para que a janela se ajuste

        chrome_windows = gw.getWindowsWithTitle('Google')
        if chrome_windows:  # se houver janelas do Chrome abertas
            try:
                # Aqui estamos substituindo chrome_windows[0].focus() pela biblioteca pywinauto
                window_title = navegador.title  # Obtemos o título da janela
                app = Application(backend="uia").connect(title_re=window_title)  # Conectamos à janela usando o título
                app.window().set_focus()  # Trazemos a janela para o primeiro plano
            except Exception as e:
                print(f'Não foi possível ativar a janela: {e}')

        # Maximiza a janela novamente
        navegador.maximize_window()
        return True