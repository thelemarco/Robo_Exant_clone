import tkinter as tk
from PIL import Image, ImageTk

# Criar janela
janela = tk.Tk()
janela.title("Integra")

imagem = Image.open(r"C:\Users\01467856738\PycharmProjects\Robo_Exant\g31160.png")
imagem = imagem.resize((110, 60))

# Converter imagem para o formato Tkinter
imagem_tk = ImageTk.PhotoImage(imagem)

# Criar rótulo para exibir a imagem
label_imagem = tk.Label(janela, image=imagem_tk)
label_imagem.grid(row=0, column=2, columnspan=2, padx=10, pady=10)

# Criar rótulos
login_label = tk.Label(janela, text="Login SEI:")
login_label.grid(row=1, column=1, padx=50, pady=10)

senha_sei = tk.Label(janela, text="Senha SEI:")
senha_sei.grid(row=2, column=1, padx=10, pady=10)

resultado_label = tk.Label(janela, text="")
resultado_label.grid(row=4, column=1, columnspan=2, padx=10, pady=10)

senha_siape = tk.Label(janela, text="Senha SIAPE:")
senha_siape.grid(row=3, column=1, padx=10, pady=10)

# Criar campos de entrada
login_entry = tk.Entry(janela)
login_entry.grid(row=1, column=2, padx=10, pady=10)

senha_entry_sei = tk.Entry(janela, show="*")
senha_entry_sei.grid(row=2, column=2, padx=10, pady=10)

senha_entry_siape = tk.Entry(janela, show="*")
senha_entry_siape.grid(row=3, column=2, padx=10, pady=10)

# Criar botão de login
login_button = tk.Button(janela, text="Iniciar")
login_button.grid(row=5, column=2, columnspan=2, padx=50, pady=10)

# Definir tamanho inicial da janela
largura = 400
altura = 600
janela.geometry(f"{largura}x{altura}")
janela.resizable(width=False, height=False)

area_destacada = tk.Frame(janela, bd=2, relief="groove")
area_destacada.place(x=30, y=320, width=330, height=250)

# Adicione widgets ou informações dentro do Frame
label_info = tk.Label(area_destacada, text="Informações aqui")

label_info.pack()


# Executar janela
janela.mainloop()
