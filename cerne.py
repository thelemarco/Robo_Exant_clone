import timeit
import sys
import time
from selenium.common.exceptions import NoAlertPresentException
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from sei import Sei_Geral
from classes_apoio import Util_suport
from loginSiape import Acessa_siape
from siape import SiapeGeral

class Principal:
    def principal(self, login_entry, senha_entry_sei, callback=None):
        start_time2 = timeit.default_timer()

        def send_message(message, message_type='info'):
            if callback is not None:
                if isinstance(message, list):
                    callback([(message, message_type)])
                else:
                    callback([(message, message_type)])

        total_de_etapas = 10
        etapa_atual = 0

        send_message(["Conectando ao SEI e Validando Credenciais"], 'info')
        servico = Service(ChromeDriverManager().install())
        options = ChromeOptions()
        options.add_experimental_option("detach", True)
        navegador = webdriver.Chrome(service=servico, options=options)
        navegador.implicitly_wait(20)
        navegador.maximize_window()

        sei = Sei_Geral()
        apoio = Util_suport()



        # Acesso ao SEI
        link = "https://sei.economia.gov.br/sip/modulos/MF/login_especial/login_especial.php?sigla_orgao_sistema=ME&sigla_sistema=SEI"

        apoio.verificar_servico(link)

        navegador.get(link)  # Acessa página inicial do SEI
        campo_usu = navegador.find_element(By.ID, 'txtUsuario').send_keys(login_entry)  # Entra Usuário
        camp_senha = navegador.find_element(By.ID, 'pwdSenha').send_keys(senha_entry_sei)  # Entra Senha
        navegador.find_element(By.XPATH, '//*[@id="selOrgao"]').send_keys('MGI')  # Escolhe Ministério
        time.sleep(0.5)
        navegador.find_element(By.XPATH, '//*[@id="Acessar"]').click()  # Acessa o SEI

        try:
            time.sleep(2)  # Espera por 2 segundos para permitir que qualquer alerta seja aberto
            alert = navegador.switch_to.alert
            send_message(["Usuário ou Senha Inválida", "Tente Novamente"], 'error')
            alert.accept()  # Você pode aceitar o alerta
            navegador.quit()
            return
        except NoAlertPresentException:
            send_message(["Credenciais do SEI válidas"], 'info')

        time.sleep(4)

        sei.tela_aviso(navegador)

        sei.sel_unidade(navegador)

        sei.visua_detal(navegador)

        send_message(["Buscando processos com Marcadores"], 'info')

        sei.procura_marcador(navegador)
        visualisa_sei = Util_suport()
        count = len(navegador.find_elements(By.CLASS_NAME, "processoVisualizado"))
        ja_conectou = False
        i = 0
        #while i < count:
        while navegador.find_element(By.CLASS_NAME, "processoVisualizado"):
            #processos_Sei = navegador.find_elements(By.CLASS_NAME, "processoVisualizado")
            try:
                start_time = timeit.default_timer()
                navegador.find_element(By.CLASS_NAME, "processoVisualizado").click()
                processo_atual = sei.num_processo(navegador)
                send_message(["Trabalhando no Processo: " + processo_atual], 'info')
                print("Ponto de controle1")
                dados_cap = sei.captura_dados(navegador, processo_atual)

                if dados_cap == False:
                    send_message(["Planilha não encontrada"], 'error')
                    time.sleep(1)
                    i += 1
                    end_time = timeit.default_timer()
                    send_message([f"Tempo de execução {round(end_time - start_time)} segundos"], 'info')
                    continue
                elif dados_cap == "modulo":
                    send_message(["Processo já lançado"], 'error')
                    time.sleep(1)
                    i += 1
                    end_time = timeit.default_timer()
                    send_message([f"Tempo de execução {round(end_time - start_time)} segundos"], 'info')
                    continue

                send_message(["Informações lidas com sucesso"], 'info')

                time.sleep(5)

                if not ja_conectou:
                    send_message(["Acessando SIAPE"], 'info')
                    acessa_siape = Acessa_siape()
                    send_message(["Validando Certificado Digital"], 'info')
                    acessa_siape.login_siape()
                    siape_geral = SiapeGeral()
                    send_message(["Abrindo Terminal"], 'info')
                    siape_geral.inicia_siape()
                    ja_conectou = True

                trocahab = SiapeGeral()
                print("Ponto de controle6")

                if trocahab.troca_hab(dados_cap['orgao'], dados_cap['upag']):
                    send_message(["UPAG verificada"], 'info')
                print("Ponto de controle7")
                griapro = SiapeGeral()
                send_message(["Gerando Processo Administrativo"], 'info')
                dv = griapro.griaproadm(dados_cap['uniProc'], dados_cap['idProc'], dados_cap['anoProc'], dados_cap['objeto'], dados_cap['descricao'])
                if dv == "M024- OPCAO EM MANUTENCAO":
                    send_message(["GRIAPROADM em Manutenção"], 'error')
                    send_message(["Verifique se a Folha está em processamento"], 'error')
                    send_message(["O INTEGRA - CGPAG será encerrado em 30s"], 'error')
                    time.sleep(30)
                    sys.exit()
                send_message(["Realizando lançamento em módulo"], 'info')
                grinbenf = SiapeGeral()
                pasta, arquiv = grinbenf.grinbenef(dados_cap, dv)
                if pasta == "Beneficiário já incluído":
                    send_message(["Beneficiário já incluído"], 'error')
                    primeiro_plano = visualisa_sei.primeiro_plano(navegador)
                    end_time = timeit.default_timer()
                    send_message([f"Tempo de execução {round(end_time - start_time)} segundos"], 'info')
                    i += 1
                    navegador.switch_to.default_content()
                    navegador.switch_to.frame('ifrArvore')
                    navegador.find_element(By.CLASS_NAME, 'noVisitado').click()
                    navegador.switch_to.default_content()
                    navegador.switch_to.frame('ifrVisualizacao')
                    navegador.find_element(By.XPATH, '//*[@id="divArvoreAcoes"]/a[25]/img').click()
                    continue

                elif arquiv == "e":
                    send_message([pasta], 'error')
                    end_time = timeit.default_timer()
                    send_message([f"Tempo de execução {round(end_time - start_time)} segundos"], 'info')
                    navegador.switch_to.default_content()
                    navegador.switch_to.frame('ifrArvore')
                    navegador.find_element(By.CLASS_NAME, 'noVisitado').click()
                    navegador.switch_to.default_content()
                    navegador.switch_to.frame('ifrVisualizacao')
                    navegador.find_element(By.XPATH, '//*[@id="divArvoreAcoes"]/a[25]/img').click()
                    i += 1
                    continue

                elif pasta == "Verificar inconsistência Matrícula X Beneficiário":
                    send_message(["Verificar inconsistência Matrícula X Beneficiário"], 'error')
                    primeiro_plano = visualisa_sei.primeiro_plano(navegador)
                    end_time = timeit.default_timer()
                    send_message([f"Tempo de execução {round(end_time - start_time)} segundos"], 'info')
                    navegador.switch_to.default_content()
                    navegador.switch_to.frame('ifrArvore')
                    navegador.find_element(By.CLASS_NAME, 'noVisitado').click()
                    navegador.switch_to.default_content()
                    navegador.switch_to.frame('ifrVisualizacao')
                    navegador.find_element(By.XPATH, '//*[@id="divArvoreAcoes"]/a[25]/img').click()
                    i += 1
                    continue

                elif pasta == "Divergência dados beneficiário":
                    send_message(["Divergência dados beneficiário"], 'error')
                    primeiro_plano = visualisa_sei.primeiro_plano(navegador)
                    end_time = timeit.default_timer()
                    send_message([f"Tempo de execução {round(end_time - start_time)} segundos"], 'info')
                    i += 1
                    navegador.switch_to.default_content()
                    navegador.switch_to.frame('ifrArvore')
                    navegador.find_element(By.CLASS_NAME, 'noVisitado').click()
                    navegador.switch_to.default_content()
                    navegador.switch_to.frame('ifrVisualizacao')
                    navegador.find_element(By.XPATH, '//*[@id="divArvoreAcoes"]/a[25]/img').click()
                    continue

                send_message(["Incluído em módulo com sucesso"], 'info')

                primeiro_plano = visualisa_sei.primeiro_plano(navegador)
                send_message(["Incluindo comprovante no SEI"], 'info')
                comprovante = sei.inclui_comprov(navegador, arquiv)
                time.sleep(1)
                send_message(["Comprovante incluído no SEI"], 'info')

                # Chama Nota Técnica
                numSiape = dados_cap['process_parte1'] + "." + dados_cap['process_parte2'] + "/" + dados_cap['anoProc'] \
                           + "-" + dv
                dados_nottec = {'numSei': processo_atual, 'numSiape': numSiape, 'objeto': dados_cap['objeto'],
                                'benefs':dados_cap['nome_benes'], 'orgao': dados_cap['orgao'],
                                'mat_int': dados_cap['matBene'], 'meses': dados_cap['mesFol'],
                                'anos': dados_cap['compFol'], 'total': dados_cap['total'], 'sit_func': dados_cap['sit_func']}

                nota_tecnica = sei.inclui_nota_tecnica(navegador, dados_nottec)
                navegador.switch_to.default_content()
                navegador.switch_to.frame('ifrArvore')
                time.sleep(0.5)
                navegador.find_element(By.CLASS_NAME, 'noVisitado').click()

                navegador.switch_to.default_content()
                navegador.switch_to.frame('ifrVisualizacao')
                navegador.find_element(By.XPATH, '//*[@id="divArvoreAcoes"]/a[25]/img').click()
                navegador.switch_to.default_content()

                end_time = timeit.default_timer()
                send_message([f"Tempo de execução {round(end_time - start_time)} segundos"], 'info')


            except Exception as e:
                print(f'Erro ao clicar no elemento: {e}')
            i += 1
        end_time2 = timeit.default_timer()
        elapsed_time = end_time2 - start_time2
        minutes, seconds = divmod(elapsed_time, 60)
        send_message([f"{i} processos em {int(minutes)}min{int(seconds)}s"], 'info')
        time.sleep(180)
        sys.exit()




















