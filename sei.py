import re
import sys
import time
import random
import locale
from datetime import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from siape import SiapeGeral
from classes_apoio import Util_suport
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from num2words import num2words


class Sei_Geral:
    def tempo_aleatorio(self):
        tempo = random.randint(2, 4)
        return tempo
    def tela_aviso(self, navegador):
        navegador.implicitly_wait(30)
        if navegador.find_element(By.XPATH, '/html/body/div[7]/div[2]/div[1]/div[3]/img'):  # Fecha Janela de aviso
            navegador.find_element(By.XPATH, '/html/body/div[7]/div[2]/div[1]/div[3]/img').click()

    def sel_unidade(self, navegador):
        # Clica em selação da Unidade

        navegador.implicitly_wait(30)
        navegador.find_element(By.XPATH, '/html/body/div[1]/nav/div/div[3]/div[2]/div[3]/div/a').click()
        time.sleep(self.tempo_aleatorio())
        navegador.find_element(By.XPATH, '//*[@id="selInfraOrgaoUnidade"]').send_keys('MGI')  # Escolhe Unidade
        time.sleep(self.tempo_aleatorio())
        botao = navegador.find_element(By.XPATH, '//*[@id="chkInfraItem0"]').get_attribute(
            'checked')  # Verifica se a unidade está selecionada
        if botao != 'true':
            navegador.find_element(By.XPATH, '/html/body/div[1]/div/div[2]/form/div[3]/table/tbody/tr[2]/td[2]').click()
            time.sleep(self.tempo_aleatorio())
        else:
            navegador.find_element(By.XPATH, '/html/body/div[1]/nav/div/div[3]/div[2]/div[4]/a/img').click()
            time.sleep(self.tempo_aleatorio())

    def visua_detal(self, navegador):
        # Seleciona a visualização detalhada no controle de processos
        time.sleep(self.tempo_aleatorio())

        navegador.find_element(By.XPATH, '/html/body/div[1]/div/div[2]/form/div/div[4]/div[1]/a').click()
        time.sleep(self.tempo_aleatorio())

    def procura_marcador(self, navegador):
        # Seleciona a visualização por marcadores
        navegador.implicitly_wait(30)
        navegador.find_element(By.XPATH, '//*[@id="divFiltro"]/div[4]/a').click()
        time.sleep(self.tempo_aleatorio())

        # Seleciona marcador

        navegador.find_element(By.XPATH, '//*[@onclick="filtrarMarcador(71471)"]').click()
        time.sleep(self.tempo_aleatorio())

        # navegador.find_element(By.CSS_SELECTOR, '#tblProcessosDetalhado > tbody > tr:nth-child(1) > th:nth-child(10) > div > div:nth-child(2) > a > img').click()
        # time.sleep(1)

    def procura_proc_esp(self, navegador):
        # Busca por um processo específico
        tempo = random.randint(3, 7)
        navegador.implicitly_wait(30)
        navegador.find_element(By.XPATH, '//*[@id="txtPesquisaRapida"]').click()
        busca_proc = navegador.find_element(By.XPATH, '//*[@id="txtPesquisaRapida"]')
        busca_proc.send_keys('14021.124589/2023-00')
        time.sleep(tempo)
        busca_proc.send_keys(Keys.ENTER)

    # def acessa_processo:(processo):
    #     #Acessa processo da lista
    #
    #
    # def busca_planilha(self):

    def num_processo(self, navegador):
        navegador.implicitly_wait(30)
        navegador.switch_to.frame('ifrArvore')
        process = navegador.find_element(By.CLASS_NAME, 'infraArvoreNoSelecionado').text.strip()
        time.sleep(self.tempo_aleatorio())
        return process

    def captura_dados(self, navegador, process):
        print("Ponto de controle2")
        apoio = Util_suport()
        print("Ponto de controle3")
        if navegador.find_elements(By.XPATH, '//img[@title="Abrir todas as Pastas"]'):
            navegador.implicitly_wait(30)
            navegador.find_element(By.XPATH, '//img[@title="Abrir todas as Pastas"]').click()
            #time.sleep(self.tempo_aleatorio())
        print("Ponto de controle4")
        if navegador.find_elements(By.PARTIAL_LINK_TEXT, "Comprovante de lan"):

            navegador.switch_to.default_content()
            navegador.switch_to.frame('ifrVisualizacao')
            texto_marcador = "Processo já lançado em módulo"
            troca = self.troca_marcador(navegador, texto_marcador)

            navegador.find_element(By.XPATH, '//*[@id="divArvoreAcoes"]/a[25]/img').click()
            #time.sleep(self.tempo_aleatorio())
            navegador.switch_to.default_content()
            message = "modulo"
            return message
        print("Ponto de controle5")
        # Verífica a existencia de Planilha de Cálculo.
        if navegador.find_elements(By.PARTIAL_LINK_TEXT, "Planilha de Cálculo"):
            # navegador.implicitly_wait(3)

            meses = []
            anos = []
            valores = []
            numero_esquerda = []
            numero_direita = []
            cota_esquerda = []
            cota_direita = []
            nome_benes = []
            mat_bene = []
            cot_parte = []
            mat_inst = ""
            valor2 = 0
            valor3 = 0
            mes_anterior = None
            ano_anterior = None

            plan = navegador.find_elements(By.PARTIAL_LINK_TEXT, "Planilha de Cálculo")
            plan[-1].click()
            navegador.switch_to.default_content()
            navegador.switch_to.frame('ifrVisualizacao')
            navegador.switch_to.frame('ifrArvoreHtml')
            assunto = navegador.find_element(By.CSS_SELECTOR,
                                             "body > table:nth-child(6) > tbody > tr:nth-child(1) > td:nth-child(2)").get_attribute(
                'textContent').strip()
            objeto = apoio.extrair_numerais(navegador.find_element(By.CSS_SELECTOR,
                                                                   "body > table:nth-child(6) > tbody > tr:nth-child(2) > td:nth-child(2)").get_attribute(
                'textContent')).strip()
            descricao = navegador.find_element(By.CSS_SELECTOR,
                                               "body > table:nth-child(6) > tbody > tr:nth-child(4) > td:nth-child(2)").get_attribute(
                'textContent').strip()
            nome_inst = navegador.find_element(By.CSS_SELECTOR,
                                               "body > table:nth-child(8) > tbody > tr:nth-child(3) > td:nth-child(2)").get_attribute(
                'textContent').strip()
            sit_func = navegador.find_element(By.CSS_SELECTOR,
                                              "body > table:nth-child(8) > tbody > tr:nth-child(2) > td:nth-child(2)").get_attribute(
                'textContent').strip()
            orgao1 = navegador.find_element(By.CSS_SELECTOR,
                                            "body > table:nth-child(8) > tbody > tr:nth-child(4) > td:nth-child(2)").get_attribute(
                'textContent').strip()
            upag = navegador.find_element(By.CSS_SELECTOR,
                                          "body > table:nth-child(8) > tbody > tr:nth-child(5) > td:nth-child(2)").get_attribute(
                'textContent').strip()
            ultim_carac = upag[-1]
            upag1 = '0' * 8 + ultim_carac
            mat_inst = navegador.find_element(By.CSS_SELECTOR,
                                              "body > table:nth-child(8) > tbody > tr:nth-child(6) > td:nth-child(2)").get_attribute(
                'textContent').strip()

            tabela = navegador.find_element(By.XPATH, '/html/body/table[5]')

            total = 0
            linhas = tabela.find_elements(By.TAG_NAME, 'tr')

            num_linhas_processadas = 0

            for linha in linhas:
                # Ignorar as duas primeiras linhas (cabeçalho)
                if num_linhas_processadas < 2:
                    num_linhas_processadas += 1
                    continue

                # Extrair as células da linha
                celulas = linha.find_elements(By.TAG_NAME, 'td')

                if celulas[0].text.strip() == "TOTAL":

                    tot_tab = float(apoio.converter_string_para_float(celulas[1].text.strip()))
                    break  # Sai do loop se a célula [0] contém "TOTAL"

                if all(not celula.text.strip() for celula in celulas):
                    continue  # Pula para a próxima iteração se a linha estiver vazia

                mes = celulas[0].text.strip()
                ano = celulas[1].text.strip()
                valor = celulas[4].text.strip()
                valor2 = apoio.converter_string_para_float(valor)

                valEsq, valDir = apoio.converter_string_para_strings(valor2)
                # total += classes_apoio.Convert_String_To_Float.converter_string_para_float(valor)

                # Verificar se o mês e ano são iguais aos do registro anterior
                if mes == mes_anterior and ano == ano_anterior:
                    message = "Erro: Mês está duplicado."
                    return message

                # Adicionar os valores às listas correspondentes
                meses.append(mes)
                anos.append(ano)
                valores.append(valor)
                valor3 = float(valor3) + float(valor2)
                numero_esquerda.append(valEsq)
                numero_direita.append(valDir)

            tabela2 = navegador.find_element(By.XPATH, '/html/body/table[6]')

            total2 = 0
            linhas2 = tabela2.find_elements(By.TAG_NAME, 'tr')

            num_linhas_processadas2 = 0

            for linha2 in linhas2:
                # Ignorar as duas primeiras linhas (cabeçalho)
                if num_linhas_processadas2 < 2:
                    num_linhas_processadas2 += 1
                    continue

                # Extrair as células da linha
                celulas2 = linha2.find_elements(By.TAG_NAME, 'td')

                if all(not celula2.text.strip() for celula2 in celulas2):
                    continue  # Pula para a próxima iteração se a linha estiver vazia

                nome_bene = celulas2[0].text.strip()
                matinee = celulas2[1].text.strip()
                cot_parte2 = celulas2[2].text.strip()
                esquerdaCot, direitaCot = apoio.converter_string_para_strings(cot_parte2)
                info_list = [matinee, cot_parte2]

                # Adicionar os valores às listas correspondentes
                nome_benes.append(nome_bene)
                mat_bene.append(matinee)
                cota_esquerda.append(esquerdaCot)
                cota_direita.append(direitaCot)

                time.sleep(2)
            folha_comp = [folha + str(comp) for folha, comp in zip(meses, anos)]

            process_limpo = re.sub(r'[^a-zA-Z0-9]', '', process)
            process_limpo = apoio.remover_posicoes(process_limpo, [-1, -1, -3, -3])
            process_parte1 = apoio.remover_posicoes(process_limpo, [5, 6, 7, 8, 9, 10, 11, 12])
            process_parte2 = apoio.remover_posicoes(process_limpo, [0, 1, 2, 3, 4, 11, 12])
            process_parte3 = apoio.remover_posicoes(process_limpo, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

            if valor3 != tot_tab:
                message = "Valores Divergentes na Planilha"
                return message

            infoExtant = {'uniProc': process_parte1, 'idProc': process_parte2, 'anoProc': process_parte3,
                          'assunto': assunto, 'objeto': objeto, 'descricao': descricao,
                          'sit_func': sit_func, 'orgao': orgao1, 'upag': upag1, 'nome_inst': nome_inst,
                          'matInst': mat_inst,
                          'mesFol': meses, 'compFol': anos, 'nome_benes': nome_benes, 'matBene': mat_bene,
                          'valorEsq': numero_esquerda,
                          'valorDir': numero_direita, 'cotEsq': cota_esquerda, 'cotDir': cota_direita, 'total': tot_tab}


            return infoExtant


        else:

            navegador.switch_to.default_content()
            navegador.switch_to.frame('ifrVisualizacao')
            texto_marcador = "Planilha não encontrada"
            self.troca_marcador(navegador, texto_marcador)
            navegador.find_element(By.XPATH, '//*[@id="divArvoreAcoes"]/a[25]/img').click()
            #time.sleep(self.tempo_aleatorio())
            navegador.switch_to.default_content()
            message = False
            return message
            sys.exit()

    def inclui_comprov(self, navegador, arquiv):

        navegador.switch_to.default_content()
        navegador.switch_to.frame('ifrVisualizacao')
        navegador.find_element(By.XPATH, '//*[@id="divArvoreAcoes"]/a[1]/img').click()
        time.sleep(self.tempo_aleatorio())
        navegador.find_element(By.XPATH, '//*[@id="tblSeries"]/tbody/tr[1]/td/a[2]').click()
        time.sleep(self.tempo_aleatorio())
        navegador.find_element(By.XPATH, '//*[@id="selSerie"]').send_keys('Comprovante')

        navegador.find_element(By.XPATH, '//*[@id="txtDataElaboracao"]').send_keys(datetime.now().strftime('%d/%m/%Y'))

        navegador.find_element(By.XPATH, '//*[@id="txtNomeArvore"]').send_keys(
            ' de lançamento em módulo')

        navegador.find_element(By.XPATH, '//*[@id="divOptNato"]/div/label').click()
        time.sleep(self.tempo_aleatorio())
        navegador.find_element(By.XPATH, '//*[@id="divOptRestrito"]/div/label').click()
        time.sleep(self.tempo_aleatorio())
        drop = Select(navegador.find_element(By.XPATH, '//*[@id="selHipoteseLegal"]'))

        drop.select_by_visible_text("Informação Pessoal (Art. 31 da Lei nº 12.527/2011)")

        navegador.find_element(By.XPATH, '//*[@id="lblArquivo"]').click()
        time.sleep(self.tempo_aleatorio())

        arquivo = arquiv
        escolhe_arquivo = SiapeGeral()
        salva_arquivo = escolhe_arquivo.insere_comprovante(arquivo)
        navegador.find_element(By.XPATH, '//*[@id="btnSalvar"]').click()
        time.sleep(self.tempo_aleatorio())

        return True

    def inclui_nota_tecnica(self, navegador, dados_nottec):

        navegador.switch_to.frame('ifrArvore')
        protoc_comp = navegador.find_element(By.PARTIAL_LINK_TEXT, "Comprovante de lançamento em módulo").text.strip()
        convert = Util_suport()
        protocol = convert.extrair_numerais(protoc_comp)

        navegador.find_element(By.XPATH, '//*[@id="span39713917"]').click()
        time.sleep(self.tempo_aleatorio())
        navegador.switch_to.default_content()
        navegador.switch_to.frame('ifrVisualizacao')
        # navegador.switch_to.frame('ifrArvoreHtml')
        navegador.find_element(By.XPATH, '/html/body/div[1]/div/div/div[2]/div/a[1]/img').click()
        time.sleep(self.tempo_aleatorio())
        navegador.find_element(By.XPATH, '//*[@id="txtFiltro"]').send_keys('Nota Técnica', Keys.TAB)
        time.sleep(self.tempo_aleatorio())
        navegador.find_element(By.XPATH, '//*[@id="tblSeries"]/tbody/tr[11]/td/a[2]/span').click()
        time.sleep(self.tempo_aleatorio())
        navegador.find_element(By.XPATH, '//*[@id="divOptProtocoloDocumentoTextoBase"]/div/label').click()
        time.sleep(self.tempo_aleatorio())
        if dados_nottec['total'] < 30000:
            navegador.find_element(By.XPATH, '//*[@id="txtProtocoloDocumentoTextoBase"]').send_keys("35550049")
        elif dados_nottec['tot'] >= 30000:
            navegador.find_element(By.XPATH, '//*[@id="txtProtocoloDocumentoTextoBase"]').send_keys("35550639")
        time.sleep(self.tempo_aleatorio())
        navegador.find_element(By.XPATH, '//*[@id="divOptRestrito"]/div/label').click()
        time.sleep(self.tempo_aleatorio())
        drop = Select(navegador.find_element(By.XPATH, '//*[@id="selHipoteseLegal"]'))

        drop.select_by_visible_text("Informação Pessoal (Art. 31 da Lei nº 12.527/2011)")


        processo_Sei = dados_nottec['processo_atual']
        proc_Siape = dados_nottec['numSiape']
        objeto = dados_nottec['objeto']
        interessado = dados_nottec['benefs']
        orgao = dados_nottec['orgao']
        mat_int = dados_nottec['mat_int']
        meses_completos = {
            "JAN": "Janeiro", "FEV": "Fevereiro", "MAR": "Março", "ABR": "Abril",
            "MAI": "Maio", "JUN": "Junho", "JUL": "Julho", "AGO": "Agosto",
            "SET": "Setembro", "OUT": "Outubro", "NOV": "Novembro", "DEZ": "Dezembro"
        }
        mes = dados_nottec['meses']
        ano = dados_nottec['anos']
        valor = dados_nottec['total']
        mes = [meses_completos[m] for m in mes]
        if len(mes) == 1 and len(ano) == 1:
            data_texto = f"{mes[0]} de {ano[0]}"
        else:
            data_texto = f"{mes[0]} de {ano[0]} a {mes[-1]} de {ano[-1]}"
        locale.setlocale(locale.LC_ALL, 'pt_BR.utf8')
        valor_formatado = locale.currency(valor, grouping=True, symbol=True)
        valor_extenso = num2words(valor, lang='pt_BR', to='currency')
        interessados_formatados = ', '.join(
            f'{nome} - SIAPE : {matricula}' for nome, matricula in zip(interessado, mat_int))

        texto_padrao = f"Nos termos do presente expediente e em face do que consta dos autos, reconheço a dívida e autorizo o " \
                       f"pagamento do valor de {valor_formatado} ({valor_extenso}), inscrito no Módulo de Exercícios Anteriores do Sistema SIAPE, em " \
                       f"favor do(a) senhor(a) {interessados_formatados}, mediante o desbloqueio sistêmico do " \
                       f"presente Processo Administrativo no Módulo de Exercícios Anteriores do Sistema SIAPE."

        navegador.find_element(By.XPATH, '//*[@id="btnSalvar"]').click()
        time.sleep(self.tempo_aleatorio())
        janela_original = navegador.current_window_handle
        navegador.implicitly_wait(10)
        for window_handle in navegador.window_handles:
            if window_handle != janela_original:
                navegador.switch_to.window(window_handle)
                navegador.maximize_window()
                frame = navegador.find_elements(By.CLASS_NAME, 'cke_wysiwyg_frame')
                navegador.switch_to.frame(frame[7])
                time.sleep(self.tempo_aleatorio())
                navegador.find_element(By.XPATH, '/html/body/table[1]/tbody/tr[1]/td[2]').send_keys(processo_Sei,
                                                                                                    Keys.TAB, Keys.TAB)

                navegador.find_element(By.XPATH, '/html/body/table[1]/tbody/tr[2]/td[2]').send_keys(" ", proc_Siape,
                                                                                                    Keys.TAB, Keys.TAB)

                navegador.find_element(By.XPATH, '/html/body/table[1]/tbody/tr[3]/td[2]').send_keys(" ", objeto,
                                                                                                    Keys.TAB, Keys.TAB)


                navegador.find_element(By.XPATH, '/html/body/table[1]/tbody/tr[4]/td[2]').send_keys(" ", interessado,
                                                                                                    Keys.TAB, Keys.TAB)

                navegador.find_element(By.XPATH, '/html/body/table[1]/tbody/tr[5]/td[2]').send_keys(" ", orgao,
                                                                                                    Keys.TAB, Keys.TAB)

                navegador.find_element(By.XPATH, '/html/body/table[1]/tbody/tr[6]/td[2]').send_keys(" ", mat_int,
                                                                                                    Keys.TAB, Keys.TAB)
                # navegador.find_element(By.XPATH, '/html/body/table[1]/tbody/tr[7]/td[2]').clear()
                navegador.find_element(By.XPATH, '/html/body/table[1]/tbody/tr[7]/td[2]').send_keys(' PENSIONISTA')

                # navegador.find_element(By.XPATH, '/html/body/table[3]/tbody/tr/td[1]').clear()
                navegador.find_element(By.XPATH, '/html/body/table[3]/tbody/tr/td[1]').click()
                navegador.find_element(By.XPATH, '/html/body/table[3]/tbody/tr/td[1]').send_keys(" ", data_texto, Keys.TAB)
                # navegador.find_element(By.XPATH, '/html/body/table[3]/tbody/tr/td[2]').clear()
                navegador.find_element(By.XPATH, '/html/body/table[3]/tbody/tr/td[1]').send_keys(" ", valor_formatado, "(", valor_extenso, ")")

                navegador.find_element(By.XPATH, '/html/body/p[19]').clear()
                paragraph = navegador.find_element(By.XPATH, '/html/body/p[19]')
                navegador.execute_script('arguments[0].innerText = arguments[1]', paragraph, texto_padrao)

                navegador.find_element(By.XPATH, '/html/body/table[7]/tbody/tr[7]/td[2]/p').click()
                # time.sleep(100)
                navegador.switch_to.default_content()
                navegador.find_element(By.XPATH, '//*[@id="cke_491"]/span[1]').click()
                time.sleep(self.tempo_aleatorio())
                wait = WebDriverWait(navegador, 10)
                input_field = wait.until(EC.presence_of_element_located((By.XPATH, '//input[contains(@id, "cke_")]')))
                input_field.click()
                input_field.send_keys(protocol)

                ok_button = wait.until(EC.element_to_be_clickable(
                    (By.XPATH, '//span[contains(@id, "cke_") and contains(@class, "cke_dialog_ui_button")]')))
                ok_button.click()
                navegador.find_element(By.XPATH, '//*[@id="cke_455_label"]').click()
                time.sleep(2)
                navegador.close()
                navegador.switch_to.window(janela_original)
                navegador.switch_to.frame('ifrArvore')
                navegador.find_element(By.CLASS_NAME, 'noVisitado').click()

                break
    def troca_marcador(self, navegador, message):

        navegador.find_element(By.XPATH, '//*[@id="divArvoreAcoes"]/a[23]/img').click()

        table = navegador.find_element(By.TAG_NAME, "table")

        # Encontre todas as linhas da tabela
        rows = table.find_elements(By.XPATH, "//div[1]/div/div/form/div[3]/table/tbody/tr[td[2]]")

        # Percorra as linhas até encontrar o texto
        for row in rows:
            # Verifique se o texto na segunda coluna é o que estamos procurando
            if row.find_element(By.XPATH, "td[2]").text == " Robo/ Eloína":
                # Selecione o link que contém a imagem com o src especificado
                target_link = row.find_element(By.XPATH, "td[6]//a[img[@src='/infra_css/svg/remover.svg']]")

                # Clique no link
                ActionChains(navegador).move_to_element(target_link).click(target_link).perform()
                break  # Saia do loop depois de clicar

        wait = WebDriverWait(navegador, 10)
        alert = wait.until(EC.alert_is_present())
        alert.accept()

        navegador.find_element(By.XPATH, '//*[@id="btnAdicionar"]').click()
        navegador.find_element(By.XPATH, '//*[@id="selMarcador"]/div/a').click()
        navegador.find_element(By.XPATH, '//*[@id="selMarcador"]/ul/li[17]/a/label').click()
        navegador.find_element(By.XPATH, '//*[@id="txaTexto"]').send_keys(message)
        navegador.find_element(By.XPATH, '//*[@id="sbmSalvar"]').click()
        navegador.switch_to.default_content()
        navegador.switch_to.frame('ifrArvore')
        navegador.find_element(By.CLASS_NAME, 'infraArvoreNoSelecionado').click()
        navegador.switch_to.default_content()
        navegador.switch_to.frame('ifrVisualizacao')


        return True