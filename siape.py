from pywinauto import *
import os
import CisetClasses
import time
import keyboard
from pywinauto.application import Application
from pywinauto.findwindows import ElementNotFoundError
from classes_apoio import Util_suport
global intervalo
intervalo = 1

class SiapeGeral:

    def siape_manut(self):  # Nenhum parâmetro
        opcao_manut = CisetClasses.janela3270('Terminal 3270 - A - AWVAD5YL')
        time.sleep(intervalo)
        opcao_manut.ViraTelaSiafiTecla('ENTER')
        m024b = opcao_manut.PegaTextoSiafi(opcao_manut.Tela, 23, 2, 23, 26).strip()

        if m024b == "M024- OPCAO EM MANUTENCAO":

            return m024b
        else:
            return
    def inicia_siape(self):
        app = Application().start(
            r'javaws.exe -localfile "C:\Users\01467856738\AppData\LocalLow\Sun\Java\Deployment\cache\6.0\14\52bb3ace-3ce5febb"')
        time.sleep(25)
        tentativas = 0
        while not Application().connect(title='Painel de controle'):
            #alerta.exibir_alerta("Aguardando conexão com o SIAPE")
            tentativas += 1
            if tentativas >= 10:
                #alerta.exibir_alerta("Não foi possível estabelecer uma conexão com o SIAPE")
                time.sleep(10)
        app = Application().connect(title='Painel de controle')
        dlg = app['Painel de controle']
        dlg.type_keys('{TAB}')
        dlg.type_keys('{TAB}')
        dlg.type_keys('{TAB}')
        dlg.type_keys('{TAB}')
        dlg.type_keys('{TAB}')
        dlg.type_keys('{ENTER}')

        app = Application().connect(title='Terminal 3270 - A - AWVAD5YL')
        time.sleep(2)
        dlg = app['Terminal 3270 - A - AWVAD5YLTerminal 3270 - A - AWVAD5YL']
        dlg.type_keys('{F3}')
        dlg.type_keys('{F2}')
        app = Application()
        try:
            app_conn = app.connect(title='Terminal 3270 - A - AWVAD5YL')
            pid = app_conn.process
            print("A janela está presente.")

            # salva o PID para um arquivo
            with open('pid.txt', 'w') as f:
                f.write(str(pid))
        except ElementNotFoundError:
            print("A janela não está presente.")
    def troca_hab(self, orgao, upag):
        print("Ponto de controle7")
        app = Application().connect(title='Terminal 3270 - A - AWVAD5YL')
        pid = app.process
        dlg = app['Terminal 3270 - A - AWVAD5YLTerminal 3270 - A - AWVAD5YL']
        dlg.type_keys('TROCAHAB')
        time.sleep(2)
        dlg.type_keys('{ENTER}')
        orgao2 = orgao
        upag2 = upag
        orgao_upag = orgao2 + ' ' + upag2

        Acesso = CisetClasses.janela3270('Terminal 3270 - A - AWVAD5YL')
        Tela_Trocahab = Acesso.ViraTelaSiafiTecla('PF1')
        Tela_Trocahab = Acesso.ViraTelaSiafiTecla('ENTER')

        for l in range(12, 19):
            alvo = Acesso.PegaTextoSiafi(Tela_Trocahab, l, 11, l, 25)


            if alvo != orgao_upag:
                dlg.type_keys('{TAB}')
                continue
            break

        dlg.type_keys('X')

        dlg.type_keys('{ENTER}')
        dlg.type_keys('S')
        time.sleep(intervalo)
        dlg.type_keys('{ENTER}')
        dlg.type_keys('{F2}')
        return True

    def griaproadm(self, processo_parte1, processo_parte2, processo_parte3, objeto, descricao):

        app = Application().connect(title='Terminal 3270 - A - AWVAD5YL')
        time.sleep(intervalo)
        dlg = app['Terminal 3270 - A - AWVAD5YLTerminal 3270 - A - AWVAD5YL']
        dlg.type_keys('>GRIAPROADM')
        time.sleep(3)

        siape = SiapeGeral()
        resultado = siape.siape_manut()
        if resultado is not None:
            app.kill()
            return resultado

        dlg.type_keys(processo_parte1)
        dlg.type_keys(processo_parte2)
        dlg.type_keys(processo_parte3)

        griapro = CisetClasses.janela3270('Terminal 3270 - A - AWVAD5YL')
        time.sleep(intervalo)
        griapro.ViraTelaSiafiTecla('ENTER')
        proc_dv_exant = griapro.PegaTextoSiafi(griapro.Tela, 7, 45, 7, 46).strip()
        dlg.type_keys(objeto)
        texto_desc = descricao
        texto_desc = texto_desc.replace(" ", "{SPACE}")

        dlg.type_keys(texto_desc)
        dlg.type_keys('{ENTER}')
        dlg.type_keys('S')
        time.sleep(intervalo)
        dlg.type_keys('{ENTER}')

        dlg.type_keys('{F3}')
        dlg.type_keys('{F3}')
        time.sleep(intervalo)
        dlg.type_keys('{F2}')
        return proc_dv_exant

    def grinbenef(self, infoExtant, dv):

        process_parte1 = infoExtant['uniProc']
        process_parte2 = infoExtant['idProc']
        process_parte3 = infoExtant['anoProc']

        mat_inst = infoExtant['matInst']
        meses = infoExtant['mesFol']
        anos = infoExtant['compFol']
        valor_esq = infoExtant['valorEsq']
        valor_dir = infoExtant['valorDir']
        mat_bene = infoExtant['matBene']
        cotEsq = infoExtant['cotEsq']
        cotDir = infoExtant['cotDir']
        nome_inst = infoExtant['nome_inst']
        print(meses[0])
        print(anos[0])
        print(meses[-1])
        print(anos[-1])

        global file_name
        file_name = process_parte1 + process_parte2 + process_parte3 + "_" + dv

        app = Application().connect(title='Terminal 3270 - A - AWVAD5YL')
        time.sleep(intervalo)
        dlg = app['Terminal 3270 - A - AWVAD5YLTerminal 3270 - A - AWVAD5YL']
        # Entra na tela de lançamento em exercícios anteriores
        #dlg.type_keys('>GRALBENEF')
        dlg.type_keys('>GRINBENEF')
        time.sleep(intervalo)
        dlg.type_keys('{ENTER}')
        time.sleep(intervalo)
        # Entra com o número do processo e o dv
        dlg.type_keys(process_parte1)
        dlg.type_keys(process_parte2)
        dlg.type_keys(process_parte3)
        dlg.type_keys(dv)
        time.sleep(intervalo)
        dlg.type_keys('{ENTER}')

        # Entra com a matrícula do instituidor / aposentado / reformado e verifica se já há lançamento
        incluibenef = CisetClasses.janela3270('Terminal 3270 - A - AWVAD5YL')
        dlg.type_keys(mat_inst)
        time.sleep(intervalo)
        incluibenef.ViraTelaSiafiTecla('ENTER')
        captura_alerta = incluibenef.PegaTextoSiafi(incluibenef.Tela, 24, 1, 24, 80)
        if captura_alerta.isspace():
            message = captura_alerta
            dlg.type_keys('{F3}')
            dlg.type_keys('{F3}')
            time.sleep(intervalo)
            dlg.type_keys('{F2}')
            return message, "e"
        incluibenef.ViraTelaSiafiTecla('PF2')
        distancia = Util_suport()
        compara = incluibenef.PegaTextoSiafi(incluibenef.Tela, 11, 40, 11, 80)
        if not distancia.compara_nomes(nome_inst, compara):
            message = "Verificar inconsistência Matrícula X Beneficiário"
            return message, ""


        # Entra com os parametros de data inicial e data final
        dlg.type_keys(meses[0])
        dlg.type_keys(anos[0])
        dlg.type_keys(meses[-1])
        dlg.type_keys(anos[-1])
        time.sleep(intervalo)

        dlg.type_keys('{ENTER}')
        dlg.type_keys('S')
        time.sleep(intervalo)
        dlg.type_keys('{ENTER}')
        x = 0
        # Entra com os valores a serem lançados
        for indice, valor in enumerate(valor_esq):
            dlg.type_keys(valor)
            dlg.type_keys('{TAB}')

            dlg.type_keys(valor_dir[indice])
            time.sleep(0.5)

            x += 1
            if x % 10 == 0:
                dlg.type_keys('{F8}')
                dlg.type_keys('{F8}')

        time.sleep(2)
        dlg.type_keys('{ENTER}')
        dlg.type_keys('{ENTER}')
        dlg.type_keys('{ENTER}')
        time.sleep(intervalo)

        # Seleciona os beneficiários que constam na planilha
        grinbene = CisetClasses.janela3270('Terminal 3270 - A - AWVAD5YL')

        #grinbene.ViraTelaSiafiTecla('ENTER')

        time.sleep(intervalo)
        grinbene.ViraTelaSiafiTecla('PF2')
        espacos_em_branco = " " * 8  # espaços em branco consecutivos
        axisLina = 10
        contador = 0
        correspondencia_encontrada = False  # variável de controle
        max_iteracoes = 15
        iteracoes = 0
        while True:

            if iteracoes > max_iteracoes:
                print("Número máximo de iterações atingido. Quebrando o loop.")
                break
            mat_benefits = grinbene.PegaTextoSiafi(grinbene.Tela, axisLina, 6, axisLina, 13)
            print(f"'{mat_benefits}' '{mat_bene}'")

            if mat_benefits == espacos_em_branco:
                print("Encontrou espaços em branco, quebrando o loop.")
                time.sleep(intervalo)
                break  # Sai do loop se encontrar oito espaços em branco consecutivos

            if mat_benefits in mat_bene:
                for i in range(contador):
                    dlg.type_keys('{TAB}')
                dlg.type_keys('X')

                axisLina += 1
                correspondencia_encontrada = True

                time.sleep(intervalo)
            else:
                #grinbene.ViraTelaSiafiTecla('TAB')
                axisLina += 1

            iteracoes += 1

        if not correspondencia_encontrada:
            message = "Divergência dados beneficiário"
            dlg.type_keys('{F3}')
            time.sleep(intervalo)
            dlg.type_keys('{F2}')
            return message, ""
        time.sleep(2)
        grinbene.ViraTelaSiafiTecla('ENTER')

        # Lança os percentuais por beneficiário

        axisLina2 = 10
        contador2 = 0
        while True:
            mat_benefits = grinbene.PegaTextoSiafi(grinbene.Tela, axisLina2, 4, axisLina2, 11)

            if mat_benefits == espacos_em_branco:

                break  # Sai do loop se encontrar oito espaços em branco consecutivos

            if mat_benefits in mat_bene:

                indices_2 = mat_bene.index(mat_benefits)

                dlg.type_keys(cotEsq[indices_2])

                if cotEsq[indices_2] != "100":
                    for i in range(0, contador2 * 3, 3):
                        dlg.type_keys('{TAB}' * (i // 3))

                    dlg.type_keys('{TAB}')

                dlg.type_keys(cotDir[indices_2])
                dlg.type_keys('{TAB}')
                axisLina2 += 1

            else:

                grinbene.ViraTelaSiafiTecla('TAB')
                dlg.type_keys('{TAB}')
                axisLina2 += 1

        time.sleep(2)
        dlg.type_keys('{ENTER}')
        #sys.exit()

        dlg.type_keys('C')
        dlg.type_keys('{ENTER}')

        time.sleep(3)
        # Localizar a janela pelo título
        window_title = "Terminal 3270 - A - AWVAD5YL"
        app = Application().connect(title=window_title)

        # Obter a janela
        window = app.window(title=window_title)

        # Ativar a janela
        window.set_focus()

        # Pressionar a tecla Alt para abrir o menu
        keyboard.press("alt")
        keyboard.press("a")
        time.sleep(intervalo)
        keyboard.release("a")
        keyboard.release("alt")

        # Pressionar as teclas de seta para navegar até o menu "Arquivo"
        keyboard.press("Right")
        time.sleep(intervalo)
        keyboard.release("Right")

        # Pressionar as teclas de seta para navegar até o subitem de menu "Imprimir Tela"
        keyboard.press("Down")
        time.sleep(intervalo)
        keyboard.release("Down")

        # Pressionar as teclas de seta para navegar até o subitem de menu "Imprimir Tela"
        keyboard.press("Down")
        time.sleep(intervalo)
        keyboard.release("Down")

        # Pressionar as teclas de seta para navegar até o subitem de menu "Imprimir Tela"
        keyboard.press("Down")
        time.sleep(intervalo)
        keyboard.release("Down")

        # Pressionar as teclas de seta para navegar até o subitem de menu "Imprimir Tela"
        keyboard.press("Down")
        time.sleep(intervalo)
        keyboard.release("Down")

        # Pressionar as teclas de seta para navegar até o subitem de menu "Imprimir Tela"
        keyboard.press("Down")
        time.sleep(intervalo)
        keyboard.release("Down")

        # Pressionar a tecla Enter para selecionar o subitem de menu "Imprimir Tela"
        keyboard.press("Enter")
        time.sleep(intervalo)
        keyboard.release("Enter")
        time.sleep(1)

        window_title = "Terminal 3270 - A - AWVAD5YL"
        app = Application().connect(title=window_title)

        # Obter a janela
        window = app.window(title=window_title)

        # Ativar a janela
        window.set_focus()

        # Pressionar a tecla Enter para selecionar o subitem de menu "Imprimir Tela"
        keyboard.press("Enter")
        time.sleep(intervalo)
        keyboard.release("Enter")

        time.sleep(3)
        mat_inst = infoExtant['matInst']
        app = Application().connect(title=u'Salvar Saída de Impressão como', timeout=5)
        dlg = app[u'Salvar Saída de Impressão como']

        arquiv = "Comprovante de Lançamento em módulo_" + mat_inst + ".pdf"
        arquiv = arquiv.replace(" ", "{SPACE}")
        caminho_script_atual = os.path.abspath(os.path.dirname(__file__))
        pasta = os.path.join(caminho_script_atual, 'pdfs')

        caminho = os.path.join(pasta, arquiv)
        dlg.type_keys(caminho)
        time.sleep(0.5)
        dlg.type_keys('{ENTER}')
        time.sleep(0.5)

        app = Application().connect(title='Terminal 3270 - A - AWVAD5YL')
        dlg = app['Terminal 3270 - A - AWVAD5YLTerminal 3270 - A - AWVAD5YL']
        dlg = app.top_window()
        dlg.type_keys('{F3}')
        time.sleep(intervalo)
        dlg.type_keys('{F3}')
        time.sleep(intervalo)
        dlg.type_keys('{F3}')
        time.sleep(intervalo)
        dlg.type_keys('{F2}')
        time.sleep(intervalo)

        return pasta, arquiv

    def insere_comprovante(self, arquiv):

        app = Application().connect(path="C:\Program Files\Google\Chrome\Application\chrome.exe")
        time.sleep(0.5)
        dlg = app.window(title="Abrir")
        time.sleep(0.5)

        # Obter o caminho absoluto do script Python atual
        caminho_script_atual = os.path.abspath(os.path.dirname(__file__))

        # Concatenar com o subdiretório 'pdfs'
        pasta = os.path.join(caminho_script_atual, 'pdfs')
        caminho = os.path.join(pasta, arquiv)
        dlg.type_keys(caminho)
        time.sleep(0.5)
        # dlg3.type_keys('{TAB}')
        # dlg3.type_keys('{TAB}')
        dlg.type_keys('{ENTER}')
        time.sleep(0.5)
        return True

