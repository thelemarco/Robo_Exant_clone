import timeit
import sys
import time
import random

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


servico = Service(ChromeDriverManager().install())
options = ChromeOptions()
options.add_experimental_option("detach", True)
navegador = webdriver.Chrome(service=servico, options=options)
navegador.implicitly_wait(20)
navegador.maximize_window()
link = "https://sei.economia.gov.br/sip/modulos/MF/login_especial/login_especial.php?sigla_orgao_sistema=ME&sigla_sistema=SEI"
navegador.get(link)  # Acessa página inicial do SEI
campo_usu = navegador.find_element(By.ID, 'txtUsuario').send_keys("marco.aurelio-silva@gestao.gov.br")  # Entra Usuário
camp_senha = navegador.find_element(By.ID, 'pwdSenha').send_keys("Th3l3M4rc0#123")  # Entra Senha
navegador.find_element(By.XPATH, '//*[@id="selOrgao"]').send_keys('MGI')  # Escolhe Ministério
time.sleep(0.5)
navegador.find_element(By.XPATH, '//*[@id="Acessar"]').click()  # Acessa o SEI
navegador.implicitly_wait(30)
if navegador.find_element(By.XPATH, '/html/body/div[7]/div[2]/div[1]/div[3]/img'):  # Fecha Janela de aviso
    navegador.find_element(By.XPATH, '/html/body/div[7]/div[2]/div[1]/div[3]/img').click()

tempo = random.randint(1, 3)
navegador.implicitly_wait(10)
navegador.find_element(By.XPATH, '//*[@id="txtPesquisaRapida"]').click()
busca_proc = navegador.find_element(By.XPATH, '//*[@id="txtPesquisaRapida"]')
busca_proc.send_keys('14021.103287/2023-90')
time.sleep(tempo)
busca_proc.send_keys(Keys.ENTER)
navegador.switch_to.frame('ifrArvore')
navegador.find_element(By.XPATH, '/html/body/div[2]/div/form/div[1]/div/div[2]/a[59]/span').click()
navegador.switch_to.default_content()
navegador.switch_to.frame('ifrVisualizacao')
#navegador.switch_to.frame('ifrArvoreHtml')
navegador.find_element(By.XPATH, '/html/body/div[1]/div/div/div[2]/div/a[1]/img').click()
navegador.find_element(By.XPATH, '//*[@id="txtFiltro"]').send_keys('Nota Técnica', Keys.TAB)
time.sleep(tempo)
navegador.find_element(By.XPATH, '//*[@id="tblSeries"]/tbody/tr[11]/td/a[2]/span').click()
time.sleep(tempo)
navegador.find_element(By.XPATH, '//*[@id="divOptProtocoloDocumentoTextoBase"]/div/label').click()
time.sleep(tempo)
navegador.find_element(By.XPATH, '//*[@id="txtProtocoloDocumentoTextoBase"]').send_keys("35482814")
time.sleep(tempo)
navegador.find_element(By.XPATH, '//*[@id="divOptRestrito"]/div/label').click()
time.sleep(tempo)
drop = Select(navegador.find_element(By.XPATH, '//*[@id="selHipoteseLegal"]'))
time.sleep(tempo)
drop.select_by_visible_text("Informação Pessoal (Art. 31 da Lei nº 12.527/2011)")
time.sleep(tempo)


# dados_nottec = {'numSei': processo_atual, 'numSiape': numSiape, 'objeto': dados_cap['objeto'],
#                                 'benefs':dados_cap['nome_benes'], 'orgao': dados_cap['orgao'],
#                                 'mat_int': dados_cap['matBene'], 'meses': dados_cap['mesFol'],
#                                 'anos': dados_cap['compFol']}
processo_Sei = "14021.103287/2023-90"
uni_pro = "14021"
id_proc = "103287"
ano_proc = "23"
dv_proc = "00"
proc_Siape = uni_pro + "." + id_proc + "/" + ano_proc + dv_proc
objeto = "0006 - Pensão Civil"
interessado = "RAIMUNDO SEBASTIAO RIBEIRO DE CASTRO"
orgao = "40806"
mat_int = "06833560"
periodo = "Dezembro de 2022"
valor = "R$ 1.166,26 (mil cento e sessenta e seis reais e vinte e seis centavos"
texto_padrao = f"Nos termos do presente expediente e em face do que consta dos autos, reconheço a dívida e autorizo o " \
               f"pagamento do valor de {valor}, inscrito no Módulo de Exercícios Anteriores do Sistema SIAPE, em " \
                f"favor do(a) senhor(a) {interessado} - SIAPE : {mat_int}, mediante o desbloqueio sistêmico do " \
                f"presente Processo Administrativo no Módulo de Exercícios Anteriores do Sistema SIAPE."

navegador.find_element(By.XPATH, '//*[@id="btnSalvar"]').click()
time.sleep(tempo)
janela_original = navegador.current_window_handle
navegador.implicitly_wait(10)
for window_handle in navegador.window_handles:
    if window_handle != janela_original:
        navegador.switch_to.window(window_handle)
        navegador.maximize_window()
        frame = navegador.find_elements(By.CLASS_NAME, 'cke_wysiwyg_frame')
        navegador.switch_to.frame(frame[7])
        body = navegador.find_element(By.XPATH, '/html/body/table[1]/tbody/tr[1]/td[2]')
        body.clear()
        body.send_keys('tester')

        break

