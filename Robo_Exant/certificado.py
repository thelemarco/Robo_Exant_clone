import time
from pywinauto import *
from minha import senha_Siape
import pyautogui




imagem_alvo = 'ok.png'
time.sleep(3)
posicao = pyautogui.locateOnScreen(imagem_alvo)

        # Verificar se a imagem foi encontrada
if posicao is not None:
            # Obter as coordenadas do centro da imagem
    centro = pyautogui.center(posicao)
    x, y = centro

            # Clicar na posição encontrada na janela de certificados digitais
    pyautogui.click(x, y)
    time.sleep(3)
    app = Application().connect(title=u'Introduzir PIN')
    dlg = app[u'Introduzir PIN']
    time.sleep(3)
    dlg.type_keys(senha_Siape)
    dlg.type_keys('{ENTER}')
else:
    print("Imagem não encontrada na tela.")