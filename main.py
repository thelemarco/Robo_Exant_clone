from tkinter import END, WORD, scrolledtext
from tkinter.font import Font
import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk
from cerne import Principal


def iniciar_login():
    login = login_entry.get()
    senha_sei = senha_entry_sei.get()
    senha_siape = senha_entry_siape.get()
    with open('senha.txt', 'w') as file:
        file.write(senha_siape)


    obj_principal = Principal()



    def adicionar(info):
        for item in info:
            adicionar_informacao(item)
            janela.update()

    for progresso in obj_principal.principal(login, senha_sei, adicionar):
        progress_bar['value'] = progresso
        janela.update()

    # Chamar o método principal e obter os valores retornados
    variaveis = obj_principal.principal(login, senha_sei, adicionar)

    # Exibir os valores no texto
    for variavel in variaveis:
        if isinstance(variavel, list):
            for item in variavel:
                adicionar_informacao(item)
        else:
            adicionar_informacao(variavel)

    # ... e assim por diante

    # Chamar a função novamente após um segundo
    janela.after(1000, iniciar_login)

def adicionar_informacao(info):
    message, message_type = info
    if isinstance(message, list):
        message = ' '.join(message)
    if message_type == 'error':
        texto.tag_config("ERROR", foreground='#FF0000')
        texto.insert(END, message + "\n", "ERROR")
    else:
        texto.tag_config("INFO", foreground='#90EE90')
        texto.insert(END, message + "\n", "INFO")
    janela.update()


# Criar janela
janela = tk.Tk()
janela.title("Integra")
janela.attributes('-topmost', True)
janela.configure(bg="black")

cor_fonte = "white"

fonte = Font(family="Arial", size=12, weight="bold")
fonte2 = Font(family="Arial", size=10, weight="bold")

imagem = Image.open(r"logo.png")
imagem = imagem.resize((95, 112))

# Converter imagem para o formato Tkinter
imagem_tk = ImageTk.PhotoImage(imagem)

# Criar rótulo para exibir a imagem
label_imagem = tk.Label(janela, image=imagem_tk, bg="black", highlightthickness=0)
label_imagem.grid(row=0, column=0, padx=10, pady=10, columnspan=3, sticky="n")

# Criar rótulos
login_label = tk.Label(janela, text="Login SEI:", bg="black", fg="white", font=fonte)
login_label.grid(row=1, column=0, padx=10, pady=10)

senha_sei = tk.Label(janela, text="Senha SEI:", bg="black", fg="white", font=fonte)
senha_sei.grid(row=2, column=0, padx=10, pady=10)

senha_siape = tk.Label(janela, text="Senha SIAPE:", bg="black", fg="white", font=fonte)
senha_siape.grid(row=3, column=0, padx=10, pady=10)

# Criar campos de entrada
login_entry = tk.Entry(janela)
login_entry.insert(0, "marco.aurelio-silva@gestao.gov.br")
login_entry.grid(row=1, column=1, padx=10, pady=10)

senha_entry_sei = tk.Entry(janela, show="*")
senha_entry_sei.grid(row=2, column=1, padx=10, pady=10)

senha_entry_siape = tk.Entry(janela, show="*")
senha_entry_siape.grid(row=3, column=1, padx=10, pady=10)

# Continuação da segunda parte

# Adicione widgets ou informações dentro do Frame
area_destacada = tk.Frame(janela, bd=2, relief="groove", bg="black")
area_destacada.grid(row=5, column=0, columnspan=3, padx=10, pady=10, sticky="nsew")

style = ttk.Style()
style.theme_use('clam')  # Definir o tema para 'clam' (outra opção é 'default')
style.configure("Black.Horizontal.TProgressbar",
                background="black",
                troughcolor="black",
                bordercolor="black",
                lightcolor="black",
                darkcolor="black"
                )
style.layout("Black.Horizontal.TProgressbar",
             [('Horizontal.Progressbar.trough',
               {'children': [('Horizontal.Progressbar.pbar',
                              {'side': 'left', 'sticky': 'ns'})],
                'sticky': 'nswe'})])

progress_bar = ttk.Progressbar(area_destacada, style="Black.Horizontal.TProgressbar", orient="horizontal", length=350,
                               mode="determinate")
progress_bar['value'] = 0
progress_bar.pack(pady=10)

# Definir a cor do fundo da barra de progresso
s = ttk.Style()
s.configure("Black.Horizontal.TProgressbar", background="black")

texto = scrolledtext.ScrolledText(area_destacada, bg="black", wrap=WORD, font=fonte2)
texto.pack(fill=tk.BOTH, expand=True)

# Configuração do estilo das tags
texto.tag_config("vermelho", foreground="red")
texto.tag_config("verde", foreground='#90EE90')  # Define a cor verde para mensagens normais

# Criar botão para iniciar o login
botao_login = tk.Button(janela, text="Iniciar", command=iniciar_login)
botao_login.grid(row=6, column=0, columnspan=3, padx=10, pady=10)

# Configurar pesos das colunas e linhas para centralizar o conjunto
janela.grid_columnconfigure(0, weight=1)
janela.grid_columnconfigure(1, weight=1)
janela.grid_columnconfigure(2, weight=1)
janela.grid_rowconfigure(5, weight=1)

# Definir tamanho inicial da janela
largura = 400
altura = 600
janela.geometry(f"{largura}x{altura}")
janela.resizable(width=False, height=False)

# Executar janela
janela.mainloop()
